//
//  DZImageBrowerView.m
//  DZImageBrowerView
//
//  Created by admin on 16/6/8.
//  Copyright © 2016年 dzq. All rights reserved.
//

#import "DZImageBrower.h"
@interface DZImageBrower ()<iCarouselDataSource, iCarouselDelegate>
@property (nonatomic , strong) UILabel *pageLabel;
@end

@implementation DZImageBrower
-(instancetype)init{
    self = [super init];
    if (self) {
        _wrap = NO;
        _dataArray = [NSMutableArray new];
        _carousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, 0, DZ_SCREEN_WIDTH, DZ_SCREEN_HIGHT)];
        _carousel.scrollSpeed = 0.8;
        _carousel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        _carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _carousel.type = iCarouselTypeLinear;
        _carousel.delegate = self;
        _carousel.dataSource = self;
        [[[UIApplication sharedApplication] keyWindow] addSubview:_carousel];
    }
    return self;
}

#pragma mark - getter setter method

-(UILabel *)pageLabel{
    if (!_pageLabel) {
        _pageLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, DZ_SCREEN_HIGHT-36, DZ_SCREEN_WIDTH, 32)];
        _pageLabel.textAlignment = NSTextAlignmentCenter;
        _pageLabel.textColor = [UIColor whiteColor];
        _pageLabel.font = [UIFont systemFontOfSize:15.0];
        [_carousel addSubview:_pageLabel];
    }
    return _pageLabel;
}
-(void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = [dataArray mutableCopy];
    [_carousel reloadData];
}
#pragma person method

-(void)updatePageLable{
    self.pageLabel.text = [NSString stringWithFormat:@"%i/%li",[_carousel currentItemIndex]+1,(unsigned long)[_dataArray count]];
}
-(void)showPageLabel{
    [UIView animateWithDuration:2.0 animations:^{
        self.pageLabel.alpha = 1;
    }];
    [self updatePageLable];
}
-(void)hidePageLabel{
    [UIView animateWithDuration:2.0 animations:^{
        self.pageLabel.alpha = 0;
    }];
    [self updatePageLable];
}
#pragma mark - carousel delegate

- (void)carouselWillBeginDragging:(iCarousel *)carousel{
    
    [self showPageLabel];
}
-(void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
    [self hidePageLabel];
}
-(NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return [_dataArray count];
}
-(UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    if (view == nil) {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DZ_SCREEN_WIDTH, DZ_SCREEN_HIGHT)];
        ((UIImageView *)view).image = [UIImage imageNamed:_dataArray[index]];
        view.contentMode = UIViewContentModeScaleAspectFit;
        view.layer.masksToBounds = YES;
    }
    return view;
}
-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    [UIView animateWithDuration:.5 animations:^{
        [_carousel removeFromSuperview];
    }];
}
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return _wrap;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}
@end
