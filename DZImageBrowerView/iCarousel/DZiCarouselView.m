//
//  DZiCarouselView.m
//  iCarouselNoNibDemo
//
//  Created by admin on 16/6/7.
//
//
#import "DZiCarouselView.h"
@interface DZiCarouselView ()<iCarouselDataSource, iCarouselDelegate>
@property (nonatomic , strong) UILabel *pageLaebl;
@end
@implementation DZiCarouselView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _wrap = YES;
        _dataArray = [NSMutableArray new];
        _carousel = [[iCarousel alloc] initWithFrame:self.bounds];
        _carousel.backgroundColor = [UIColor clearColor];
        _carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _carousel.type = iCarouselTypeCoverFlow;
        _carousel.delegate = self;
        _carousel.dataSource = self;
        _toolView = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height-40, self.frame.size.width, 40)];
        _toolView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        
        [self addSubview:_carousel];
        UIButton *deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(16, 8, 20, 24)];
        deleteBtn.tag = 1;
        [deleteBtn setImage:[UIImage imageNamed:@"trash"] forState:UIControlStateNormal];
        [deleteBtn addTarget:self action:@selector(deleteImage) forControlEvents:UIControlEventTouchUpInside];
        UIButton *addBtn = [[UIButton alloc]initWithFrame:CGRectMake(DZ_SCREEN_WIDTH-40, 0, 40, 40)];
        addBtn.tag = 2;
        [addBtn setImage:[UIImage imageNamed:@"btn_right_add"] forState:UIControlStateNormal];
        [addBtn addTarget:self action:@selector(addImage) forControlEvents:UIControlEventTouchUpInside];
        _pageLaebl = [[UILabel alloc]initWithFrame:CGRectMake((DZ_SCREEN_WIDTH-100)/2, 4, 100, 32)];
        _pageLaebl.textAlignment = NSTextAlignmentCenter;
        _pageLaebl.textColor = [UIColor whiteColor];
        _pageLaebl.font = [UIFont systemFontOfSize:16];
        [_toolView addSubview:_pageLaebl];
        [_toolView addSubview:addBtn];
        [_toolView addSubview:deleteBtn];
        [self addSubview:_toolView];
    }
    return self;
}

-(void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = [dataArray mutableCopy];
    [_carousel reloadData];
}
-(void)deleteImage{
    if (_carousel.numberOfItems > 0)
    {
        NSInteger index = _carousel.currentItemIndex;
        [_carousel removeItemAtIndex:index animated:YES];
        [_dataArray removeObjectAtIndex:index];
        [self updatePageShow];
    }
}
-(void)addImage{
    NSInteger index = _carousel.currentItemIndex;
    [_dataArray insertObject:@"2.jpg" atIndex:index];
    [_carousel insertItemAtIndex:index animated:YES];
    [self updatePageShow];    
}
-(void)updatePageShow{
    _pageLaebl.text = [NSString stringWithFormat:@"%li/%li",[_carousel currentItemIndex]+1,[_dataArray count]];
}
#pragma mark - iCarousel delegate

-(NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return [_dataArray count];
}
-(UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
        ((UIImageView *)view).image = [UIImage imageNamed:_dataArray[index]];
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.layer.cornerRadius = 5;
        view.layer.masksToBounds = YES;
    }
    
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    
    return view;
}
-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    if (_clickAtIndex) {
        _clickAtIndex(index);
    }
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
    [self updatePageShow];
}
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return _wrap;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}
@end
