//
//  DZImageBrowerView.h
//  DZImageBrowerView
//
//  Created by admin on 16/6/8.
//  Copyright © 2016年 dzq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface DZImageBrower : NSObject
@property (nonatomic, strong) iCarousel *carousel;
@property (nonatomic , assign) BOOL wrap;
@property (nonatomic , strong) NSMutableArray * dataArray;

@end
