//
//  DZiCarouselView.h
//  iCarouselNoNibDemo
//
//  Created by admin on 16/6/7.
//
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
typedef void (^ClickAtIndex)(NSInteger index);
@interface DZiCarouselView : UIView
@property (nonatomic, strong) iCarousel *carousel;
@property (nonatomic , assign) BOOL wrap;
@property (nonatomic , strong) NSMutableArray * dataArray;
@property (nonatomic , strong) UIView *toolView;
@property (nonatomic , copy)ClickAtIndex clickAtIndex;
@end
