//
//  ViewController.m
//  DZImageBrowerView
//
//  Created by admin on 16/6/8.
//  Copyright © 2016年 dzq. All rights reserved.
//

#import "ViewController.h"
#import "DZiCarouselView.h"
#import "DZImageBrower.h"

@interface ViewController ()
@property (nonatomic , strong) DZImageBrower *imageBrowerView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    DZiCarouselView * view = [[DZiCarouselView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
    view.backgroundColor = [UIColor clearColor];
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
    effectview.frame = view.frame;
    NSArray *array = @[@"1.jpg",@"2.jpg",@"3.jpg"];
    view.dataArray = [NSMutableArray arrayWithArray:array];
    [self.view addSubview:effectview];
    [effectview.contentView addSubview:view];
    view.clickAtIndex = ^(NSInteger index){
            _imageBrowerView = [[DZImageBrower alloc]init];
        _imageBrowerView.dataArray = [NSMutableArray arrayWithArray: view.dataArray];
        [_imageBrowerView.carousel setCurrentItemIndex:index];
    };
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
